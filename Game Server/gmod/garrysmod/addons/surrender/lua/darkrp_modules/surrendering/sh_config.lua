SurrenderSettings = {}

--Functionality allowing you to decide whether or not a player should be able to surrender (prevents shouting on player aswell), returning false will stop the player from being surrendered completly
SurrenderSettings["cansurrender"] = function(ply)
	--if IsAntlion(ply) or IsAntlionG(ply) then return false end
	return true
end

--Functionality allowing you to decide whether or not a player should be able to shout
SurrenderSettings["canshout"] = function(ply,vic)
	--if IsAntlion(ply) or IsAntlionG(ply) then return false end
	--if not ply:isCP() then return false end --Because people like to do this
	--if vic:IsSuperAdmin() then return false end --Im a celebrity! Leave me alone!
	return true
end

--Functionality allowing you to decide whether or not a player can be tied. Returning false disallows it, and adding a second stringed argument will add that as a text incase things "go wrong"
SurrenderSettings["cantie"] = function(ply,vic)
	--if IsAntlion(ply) or IsAntlionG(ply) then return false, "(Antlions cant tie people.)" end
	--if IsAntlion(vic) or IsAntlionG(vic) then return false, "(Antlions cant be tied.)" end
	--if not ply:HasWeapon("weapon_lordi_rope") then return false, "(You dont have rope)" end
	return true
end

--Allow players with a valid weapon, to shout at others causing them to surrender? (Only if the victim doesnt have a "valid" weapon out aswell)
SurrenderSettings["forcesurrender"] = true

--What messages should be spoken by the player, when they shout, whilst the target is running? (Only if SurrenderSettings["forcesurrender"] is set to true)
SurrenderSettings["forcesurrender_messages_running"] = {
"Dont move!",
"Hold it!",
"DONT move",
"Stop running!",
"Hold it right there!"
}

--What messages should be spoken by the player, when they shout, whilst the target is standing still? (Only if SurrenderSettings["forcesurrender"] is set to true)
SurrenderSettings["forcesurrender_messages_standing"] = {
"Get down!",
"Kiss the floor!",
"Down!",
"On the ground!",
"Get down, on the ground!",
"How about you get down?"
}

--What messages should be spoken by the player, when they shout, whilst the target is already surrendering? (Only if SurrenderSettings["forcesurrender"] is set to true)
SurrenderSettings["forcesurrender_messages_surrendering"] = {
"And stay down!",
"Whatd i say?",
"Dont go anywhere!",
"Get up, and you get a bullet to the head!",
"See this? Im gonna introduce it to your head, if you move.",
"Stand still!",
"Dont go anywhere, pretty please."
}

--How likely is it that a player will surrender, when asked to?
--1 is always, 4 is 1/4 chance, 50 is 1/50 chance, and so on (Only if SurrenderSettings["forcesurrender"] is set to true)
SurrenderSettings["forcesurrender_chance"] = 4

--How long will a player surrender for, when asked to? (in seconds) (Only if SurrenderSettings["forcesurrender"] is set to true)
SurrenderSettings["forcesurrender_time"] = 15

--Should SurrenderSettings["intimidatingweapons"] be inverted? This will cause the table to act as a blacklist, rather than a whitelist
SurrenderSettings["intimidatingweapons_invert"] = false

--What weapons should be able to cause a player to forcibly surrender, if the wielder shouts at them? (Only if SurrenderSettings["forcesurrender"] is set to true)
SurrenderSettings["intimidatingweapons"] = {
["weapon_ak472"] = true,
["weapon_deagle2"] = true,
["weapon_fiveseven2"] = true,
["weapon_glock2"] = true,
["weapon_m42"] = true,
["weapon_mac102"] = true,
["weapon_mp52"] = true,
["weapon_p2282"] = true,
["weapon_pumpshotgun2"] = true,
["ls_sniper"] = true
}

--If a player is surrendering, they can still take damage, this will scale the damage, allowing surrendering people, to have a chance to show that they are surrendering
--so they wont get killed the minute another player spots them (set to 1 for full damage)
SurrenderSettings["surrenderdamagescale"] = 0.25

--Should SurrenderSettings["weapontostrip"] be inverted? This will cause the table to act as a blacklist, rather than a whitelist
SurrenderSettings["weapontostrip_invert"] = false

--What weapons should get striped from a player, when their weapons are "removed"? (This will cause the player "removing" the weapons, to receive them
--and if they already have it, the weapon will drop on the ground)
SurrenderSettings["weapontostrip"] = {
["weapon_ak472"] = true,
["weapon_deagle2"] = true,
["weapon_fiveseven2"] = true,
["weapon_glock2"] = true,
["weapon_m42"] = true,
["weapon_mac102"] = true,
["weapon_mp52"] = true,
["weapon_p2282"] = true,
["weapon_pumpshotgun2"] = true,
["ls_sniper"] = true
}