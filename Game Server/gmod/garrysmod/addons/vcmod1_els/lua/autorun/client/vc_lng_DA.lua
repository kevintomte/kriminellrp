// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Dansk"
Lng.Language_Code = "DA"
Lng.Translated_By_Name = "TDM"
Lng.Translated_By_Link = "http://steamcommunity.com/id/TheDanishMaster/"
Lng.Translated_Date = "2015 03 16"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Køretøj låst."
Lng.UnLocked = "Køretøj låst op."
Lng.Chat = 'Hvis du vil ændre indstillinger (lys, kamera), skal du skrive "!vcmod" i chatten.'
Lng.Broken = "Køretøjet er ødelagt og skal først repareres." 
Lng.Trl_Atch = "Trailer tilkoblet."
Lng.Trl_Detch = "Trailer afkoblet."
Lng.ELS_TuningIntoPoliceC = "Stiller over på politiradio."
Lng.ELS_NoPoliceRCFound = "Ingen politiradio blev fundet."

////////Pickup
Lng.TouchCar100 = "Ved kontakt med et køretøj repareres der +100% af det."
Lng.TouchCar25 = "Ved kontakt med et køretøj repareres der +25% af det."
Lng.TouchCar10 = "Ved kontakt med et køretøj repareres der +10% af det."

////////MENU general
Lng.Info = "Info"
Lng.Menu = "Menu"
Lng.Language = "Sprog"
Lng.Personal = "Personlig"
Lng.Administrator = "Administrator"
Lng.Options = "Indstillinger"
Lng.ELSOptions = "ELS Indstillinger"
Lng.Main = "Primær"
Lng.Controls = "Kontrolinds"
Lng.HUD = "HUD"
Lng.View = "Kameravinkel"
Lng.Radio = "Radio"
Lng.Multiplier = "Multiplikator"
Lng.OptOnly_You = "Disse indstillinger vil kun betyde noget for dig."
Lng.OptOnly_Admin = "Disse indstillinger kan kun ændres af en administrator."
Lng.NPC_Settings = "NPC indstillinger"
Lng.Height = "Højde"
Lng.FadeOutDistance = "Fade ud afstand"
Lng.Reset = "Nulstil"
Lng.Save = "Gem"
Lng.Load = "Indlæs"
Lng.Volume = "Lydstyrke"
Lng.Distance = "Afstand"
Lng.None = "Ingen"
Lng.EnterKey = "Vælg tast"

Lng.Enabled_Cl = "Slået til hos klienten"
Lng.Enabled_Sv = "Slået til hos serveren"

Lng.Lights = "Lys"
Lng.Health = "Liv"
Lng.Sound = "Lyd"
Lng.Other = "Andet"
Lng.CreatedBy = "Lavet af"
Lng.Enabled = "Slået til"
Lng.OffTime = "Slå fra efter"
Lng.Time = "Tid"
Lng.DistMultiplier = "Afstand multiplikator"
Lng.ControlsReset = "Kontrolindstillinger nulstillet."
Lng.SettingsSaved = "Indstillinger gemt."
Lng.SettingsReset = "Indstillinger nulstillet."
Lng.LoadedSettingsFromServer = "Indstillinger indlæst fra server."
Lng.InteriorIndicators = "Kabinelys"
Lng.ExtraGlow = "Ekstra skær"

////////MENU ELS
Lng.ManulSiren = "Manuel sirene"
Lng.ELS_SirenSwitch = "ELS sirene skift"
Lng.ELS_SirenToggle = "ELS sirene knap"
Lng.ELS_LightsSwitch = "ELS lys skift"
Lng.ELS_LightsToggle = "ELS lys knap"
Lng.VCModMainEnabled = "VCMod Primær slået til"
Lng.VCModELSEnabled = "VCMod ELS slået til"
Lng.ELSLightsEnabled = "ELS lys slået til"
Lng.AutoDisableELSLights = "Slå automatisk ELS lys fra"
Lng.OffOnExit = "Slået fra når køretøjet forlades"
Lng.Siren = "Sirene"
Lng.AutoDisableELSSounds = "Slå automatisk ELS lyde fra"
Lng.Manual = "Manuel"
Lng.Bullhorn = "Megafon"
Lng.PoliceChatterEnabled = "Politiradio slået til"
Lng.PoliceChatter = "Politiradio"
Lng.PoliceChatter_Info = "Politiradio er en livestream af rigtige politiradioer." 
Lng.SelectedRadioChatter = "Valgte politiradio kanal"
Lng.ReduceDamageToEmergencyVehicles = "Reducér skade til statsejede køretøjer"

////////MENU personal info
Lng.YouAreUsingVCMod = "Du bruger VCMod"
Lng.ServerIsUsingVCMod = "Serveren bruger VCMod"
Lng.Info_EverThought = "Har du nogensinde tænkt, at køretøjer i Garry's Mod manglede noget?\nVCMod er designet til at gøre køretøjer lige så gode som i andre spil."
Lng.Info_VCModHasFollowingAddons = "VCMod har følgende tilføjelser:"

////////MENU personal options
Lng.VisDist = "Sigtbarhedsafstand"
Lng.Warmth = "Varme"
Lng.Lines = "Linjer"
Lng.Glow = "Glød"
Lng.DynamicLights = "Dynamisk lys"
Lng.ThirdPView = "Tredjepersonsvinkel"
Lng.DynamicView = "Dynamisk vinkel"
Lng.AutoFocus = "Automatisk fokus"
Lng.Reverse = "Bakgear"
Lng.VectorStiffness = "Vektor stivhed i %"
Lng.AngleStiffness = "Vinkel stivhed i %"
Lng.IgnoreWorld = "Ignorér verden"
Lng.TruckView = "Tilpas kamera til tilkoblet trailer"

////////MENU controls
Lng.HoldDuration = "Hold nede i antal tid"
Lng.Mouse = "Mus"
Lng.KeyboardInput = "Tastaturinput"
Lng.MouseInput = "Museinput"

Lng.NightLights = "Nattelys"
Lng.HeadLights = "Forlygter"
Lng.LowHigh = "Nær/fjernlys skift"
Lng.HazardLights = "Havariblink"
Lng.BlinkerLeft = "Venstre blinklys"
Lng.BlinkerRight = "Højre blinklys"
Lng.Horn = "Horn"
Lng.Cruise = "Fartpilot"
Lng.LockUnlock = "Lås/Lås op"
Lng.LookBehind = "Se bagud"
Lng.DetachTrl = "Frakoble trailer"

////////MENU hud
Lng.Effect3D = "3D effekt"
Lng.HUDHeight = "HUD højde i %"
Lng.HUD_Name = "Navn"
Lng.HUD_Icons = "Ikoner"
Lng.HUD_Cruise = "Fartpilot"
Lng.HUD_Cruise_MPH = "mi/t i stedet for km/t"
Lng.HUD_Repair = "Reparér"
Lng.HUD_ELS_Siren = "ELS sirene"
Lng.HUD_ELS_Lights = "ELS lys"

////////MENU admin options
Lng.HandbrakeLights = "Håndbremselys"
Lng.InteriorLights = "Kabinelys"
Lng.BlinkersOffExit = "Blinklys bliver slået fra når køretøjet forlades"

Lng.DamageEnabled = "Skade slået til"
Lng.StartHealthMultiplier = "Start liv forstørrelse"
Lng.PhysicalDamage = "Fysisk skade"
Lng.FireDuration = "Brand varighed"
Lng.RemoveCarAfterExplosion = "Fjern køretøj efter eksplosion"
Lng.ReducePlayerDmgWhileInCar = "Reducér spillerskade mens i køretøj"

Lng.DoorSounds = "Dør lyd"
Lng.TruckRevBeep = "Lastbil bakgear lyd"

Lng.SteeringWheelLOnExit = "Lås styretøj når køretøjet forlades"
Lng.BrakesLOnExit = "Brems når køretøjet forlades"
Lng.WheelDust = "Støv fra hjul"
Lng.WheelDustWhileBraking = "Støv fra hjul når der bremses"
Lng.MatchPlayerSpeedExit = "Efterlign spillerens hastighed når køretøjet forlades"
Lng.NoCollidePlyOnExit = "Beskyt spiller når køretøjet forlades"
Lng.Exhaust = "Udstødning"
Lng.PassengerSeats = "Passagersæder"
Lng.TrailerAttach = "Tilkobl trailer"
Lng.TrailerAttachConStrengthM = "Trailer tilkobling styrke"
Lng.TrailersCanAtchToReg = "Trailere kan kobles til normale biler"
Lng.RepairToolSpeedMult = "Værktøj hastighed multiplikator"

if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng