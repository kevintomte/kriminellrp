MsgC( "[ARS] Admin Report System 1.4 by BC BEST on ScriptFodder. \n" )
util.AddNetworkString("ARS_Reopen_ToClient")

util.AddNetworkString("ARS_Request")
hook.Add( "PlayerSay", "RequestPanel", function( ply, text, to )
	if (string.lower(text) == ARS.PlayerChatCommand) then
		if ARS.OnlyAllowIfAdminOn then 
			if AnyAdmins() then 
				if ply:CanDoIt() then 
					net.Start("ARS_Request")
					net.Send(ply)
				else
					ARSNotify(ply, 0, 10, "You cannot place another report this soon!")
				end
			else
				ARSNotify(ply, 0, 10, "You cannot report. There are no admins!")
			end
		else
			if ply:CanDoIt() then 
				net.Start("ARS_Request")
				net.Send(ply)
			else
				ARSNotify(ply, 0, 10, "You cannot place another report this soon!")
			end
		end
    end
    if ply:IsARSAdmin() then
		if (string.lower(text) == ARS.AdminChatCommand) then
			local HereorNah
			net.Start("ARS_AdminPanel")
				if file.Exists( "ars_reports/savedreports.txt", "DATA" ) then 
					local FILE = file.Read( "ars_reports/savedreports.txt", "DATA" )
					TABLE = util.JSONToTable(FILE)
					HereorNah = true
				else 
					TABLE = {}
					HereorNah = false
				end
				net.WriteTable(TABLE)
				net.WriteBool(HereorNah)
			net.Send(ply)
		end
	end
end)

hook.Add( "PlayerSay", "WrongCommand", function( ply, text, to )
	if ARS.WrongCommandWarning then
		local subtxt = string.sub(text, 1, 1)
		if string.sub(ARS.PlayerChatCommand, 1, 1) != "@" then 
			if (subtxt == "@") then
				ARSNotify(ply, 1, 10, "To request an admin, you must use '"..ARS.PlayerChatCommand.."'.")
			end
		end
	end
end)

util.AddNetworkString("ARS_AdminPanel")
