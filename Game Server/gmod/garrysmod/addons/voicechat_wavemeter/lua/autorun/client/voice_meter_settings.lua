VoiceChatMeter = {}

VoiceChatMeter.IsTTT = false

VoiceChatMeter.DarkRPSelfSquare = true

VoiceChatMeter.SizeX = 250
VoiceChatMeter.SizeY = 40
VoiceChatMeter.FontSize = 17
VoiceChatMeter.Radius = 4
VoiceChatMeter.FadeAm = .1
VoiceChatMeter.SlideOut = true
VoiceChatMeter.SlideTime = .1

VoiceChatMeter.PosX = .97
VoiceChatMeter.PosY = .76
VoiceChatMeter.Align = 0
VoiceChatMeter.StackUp = true

VoiceChatMeter.UseTags = true
VoiceChatMeter.Tags = {
	["superadmin"] = "[A]",
	["admin"] = "[A]",
	["moderator"] = "[M]"
}

if (VoiceChatMeter.IsTTT) then
	VoiceChatMeter.SizeX = 220
	VoiceChatMeter.SizeY = 40
	VoiceChatMeter.PosX = .02
	VoiceChatMeter.PosY = .03
	VoiceChatMeter.Align = 1
	VoiceChatMeter.StackUp = false
end

include("voicemeter/cl_voice_meter.lua")