// Copyright © 2012-2015 VCMod (freemmaann). All Rights Reserved. if you have any complaints or ideas contact me: email - freemmaann@gmail.com or skype - comman6.
local Lng = {}

//You do not have to translate everything. Things that are not translated will be written in English.

//Set up the language
Lng.Name = "Lietuvių"
Lng.Language_Code = "LT"
Lng.Translated_By_Name = "freemmaann"
Lng.Translated_By_Link = "http://steamcommunity.com//id/freemmaann/"
Lng.Translated_Date = "2015 03 16"

////////////// Translation starts here

////////GENERAL
Lng.Locked = "Mašina užrakinta."
Lng.UnLocked = "Mašina atrakinta."
Lng.Chat = 'Norint pakeisti nustatymus (šviesas, vaizdą) rašykite "!vcmod" į chatą.'
Lng.Broken = "Mašina yra sugedusi, pirma ją reikia sutaisyti."
Lng.Trl_Atch = "Priekaba prikabinta."
Lng.Trl_Detch = "Priekaba atkabinta."
Lng.ELS_TuningIntoPoliceC = "Jungiamasi prie policijos pokalbių"
Lng.ELS_NoPoliceRCFound = "Policijos pokalbių stoties nerasta."

////////Pickup
Lng.TouchCar100 = "Palieskite automobilį kad jį sutvarkytumėte 100%."
Lng.TouchCar25 = "Prilieskite prie automobilio, kad jis būtų suremontuotas +25% maksimum galimo remonto."
Lng.TouchCar10 = "Prilieskite prie automobilio, kad jis būtų suremontuotas +10% maksimum galimo remonto."

////////MENU general
Lng.Info = "Info"
Lng.Menu = "Meniu"
Lng.Language = "Kalba"
Lng.Personal = "Asmeniniai"
Lng.Administrator = "Administratoriaus"
Lng.Options = "Nustatymai"
Lng.ELSOptions = "ELS Nustatymai"
Lng.Main = "Pagrindinis"
Lng.Controls = "Kontrolė"
Lng.HUD = "HUD"
Lng.View = "Vaizdas"
Lng.Radio = "Radijas"
Lng.Multiplier = "Daugiklis"
Lng.OptOnly_You = "Šie nustatymai paveiks tik jus"
Lng.OptOnly_Admin = "Šiuos nustatymus gali keisti tik administratorius"
Lng.NPC_Settings = "NPC nustatymai"
Lng.Height = "Auktis"
Lng.FadeOutDistance = "Išblankimo atstumas"
Lng.Reset = "Perkrauti"
Lng.Save = "Išsaugoti"
Lng.Load = "Įkelti"
Lng.Volume = "Garsas"
Lng.Distance = "Atstumas"
Lng.None = "Niekas"
Lng.EnterKey = "Paspauskinte mygtuką"

Lng.Enabled_Cl = "Įjungta lokaliai"
Lng.Enabled_Sv = "Įjungta nuotoliniai"

Lng.Lights = "Šviesos"
Lng.Health = "Sveikata"
Lng.Sound = "Garsas"
Lng.Other = "Kita"
Lng.CreatedBy = "Sukūrė"
Lng.Enabled = "Įjungta"
Lng.OffTime = "Išjungimo laikas"
Lng.Time = "Laikas"
Lng.DistMultiplier = "Atstumo duagiklis"
Lng.ControlsReset = "Kontrolė atkurta."
Lng.SettingsSaved = "Nustatymai išsaugoti."
Lng.SettingsReset = "Nustatymai atkurti."
Lng.LoadedSettingsFromServer = "Nustatymai atsiūsti iš serverio."
Lng.InteriorIndicators = "Vidiniai indikatoriai"
Lng.ExtraGlow = "Papildomas švytėjimas"

////////MENU ELS
Lng.ManulSiren = "Pauspaudžiant sirena"
Lng.ELS_SirenSwitch = "ELS sirenos perjungimas"
Lng.ELS_SirenToggle = "ELS sirenos on/off"
Lng.ELS_LightsSwitch = "ELS šviesų perjungimas"
Lng.ELS_LightsToggle = "ELS šviesų on/off"
Lng.VCModMainEnabled = "VCMod main įjungtas"
Lng.VCModELSEnabled = "VCMod ELS įjungtas"
Lng.ELSLightsEnabled = "ELS šviesos įjungtos"
Lng.AutoDisableELSLights = "Auto išjungti ELS šviesas"
Lng.OffOnExit = "Išjungti išeinant"
Lng.Siren = "Sirenos"
Lng.AutoDisableELSSounds = "Auto išjungti ELS garsus"
Lng.Manual = "Paspaudžiant"
Lng.Bullhorn = "Signalas"
Lng.PoliceChatterEnabled = "Policijos pokalbiai įjungti"
Lng.PoliceChatter = "Policijos pokalbiai"
Lng.PoliceChatter_Info = "Policijos pokalbiai yra tiesioginis policijos oficialius pokalbių transliavimas per radiją."
Lng.SelectedRadioChatter = "Pasirinktas policijos pokalbių kanalas"
Lng.ReduceDamageToEmergencyVehicles = "Sumažinti žala policijos mašinoms"

////////MENU personal info
Lng.YouAreUsingVCMod = "Jūs naudojaties VCMod"
Lng.ServerIsUsingVCMod = "Šis serveris naudoja VCMod"
Lng.Info_EverThought = "Ar kadanors esat pagalvoja, kad gmod mašinoms kažko trūktsa?\nVCMod yra sukurtas siekiant paversti jas ne ka blogesnias negu kituosia žaidimuose."
Lng.Info_VCModHasFollowingAddons = "VCMod turi šiuos papildinius:"

////////MENU personal options
Lng.VisDist = "Matomumo atstumas"
Lng.Warmth = "Šiluma"
Lng.Lines = "Linijos"
Lng.Glow = "Švytėjimas"
Lng.DynamicLights = "Dinaminės šviesos"
Lng.ThirdPView = "Trešiojo asmenis vaizdas"
Lng.DynamicView = "Dinaminis vaizdas"
Lng.AutoFocus = "Auto pritraukimas"
Lng.Reverse = "Atbulinis"
Lng.VectorStiffness = "Vectoriaus stangrumas %"
Lng.AngleStiffness = "Kampo kaitos stangrumas %"
Lng.IgnoreWorld = "Ignoruoti pasaulį"
Lng.TruckView = "Vaizdas prijungus priekabą"

////////MENU controls
Lng.HoldDuration = "Laikymo trukmė"
Lng.Mouse = "Pelė"
Lng.KeyboardInput = "Klaviatūros įvestis"
Lng.MouseInput = "Pelės įvestis"

Lng.NightLights = "Naktinės šviesos"
Lng.HeadLights = "Priekinės šviesos"
Lng.LowHigh = "Tolimosios/artimosios rėžimas"
Lng.HazardLights = "Avarinės šviesos"
Lng.BlinkerLeft = "Mirgsėjimas kairė"
Lng.BlinkerRight = "Mirgsėjimas dešinė"
Lng.Horn = "Signalas"
Lng.Cruise = "Kruizas"
Lng.LockUnlock = "Užrakinti/Atrakinta"
Lng.LookBehind = "Žiūrėti atgal"
Lng.DetachTrl = "Atkabinti priekabą"

////////MENU hud
Lng.Effect3D = "3D efektas"
Lng.HUDHeight = "Šoninės HUD aukštis %"
Lng.HUD_Name = "Pavadinimas"
Lng.HUD_Icons = "Ikonos"
Lng.HUD_Cruise = "Kruizas"
Lng.HUD_Cruise_MPH = "mi/h vietoj km/h"
Lng.HUD_Repair = "Sutvarkymas"
Lng.HUD_ELS_Siren = "ELS sirenos"
Lng.HUD_ELS_Lights = "ELS šviesos"

////////MENU admin options
Lng.HandbrakeLights = "Rankinio stabdžio šviesos"
Lng.InteriorLights = "Interiero šviesos"
Lng.BlinkersOffExit = "Mirgsėjimas išjungiamas išlipant"

Lng.DamageEnabled = "Sunaikinimas įjungtas"
Lng.StartHealthMultiplier = "Pradinės sveikatos daugiklis"
Lng.PhysicalDamage = "Fizinė žala"
Lng.FireDuration = "Ugnies trukmė"
Lng.RemoveCarAfterExplosion = "Pašalinti mašina po sprogimo"
Lng.ReducePlayerDmgWhileInCar = "Sumažinti žalą žaidejui, jam ęsant mašinoje"

Lng.DoorSounds = "Durų garsai"
Lng.TruckRevBeep = "Suknvežimio atbulinis garsas"

Lng.SteeringWheelLOnExit = "Vairo užrakinimas išlipant"
Lng.BrakesLOnExit = "Stabdžių užrakinimas išlipant"
Lng.WheelDust = "Ratų dulkės"
Lng.WheelDustWhileBraking = "Ratų dulkės stabdant"
Lng.MatchPlayerSpeedExit = "Sutapatinti žaidėjo greitį išlipant"
Lng.NoCollidePlyOnExit = "Neliesti žaidėjo su mašina išlipant"
Lng.Exhaust = "Išmetamosios dujos"
Lng.PassengerSeats = "Keleivių sedynės"
Lng.TrailerAttach = "Priekabos prikabinimas"
Lng.TrailerAttachConStrengthM = "Priekabos prikabinimo jungties stiprumas"
Lng.TrailersCanAtchToReg = "Priekabos gali prisikabinti prie lengvųjū automobilių"
Lng.RepairToolSpeedMult = "Taisymo įrankio greičio daugiklis"



if !VC_Lng_T then VC_Lng_T = {} end VC_Lng_T[Lng.Language_Code] = Lng