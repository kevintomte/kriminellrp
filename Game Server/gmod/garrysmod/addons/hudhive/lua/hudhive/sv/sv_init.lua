﻿-----------------------------------------------------------------
-- @package     HUDHive
-- @author      Richard
-- @build       v1.2.1
-- @release     12.29.2015
-----------------------------------------------------------------

HUDHive = HUDHive or {}

-----------------------------------------------------------------
-- [ RESOURCES ]
-----------------------------------------------------------------

if HUDHive.Settings.WorkshopEnabled then
	resource.AddWorkshop( "559048049" )
end

if HUDHive.Settings.ResourcesEnabled then

	resource.AddFile( "resource/fonts/adventpro_light.ttf" )
	resource.AddFile( "resource/fonts/oswald_light.ttf" )
	resource.AddFile( "resource/fonts/teko_light.ttf" )

	resource.AddFile( "materials/hudhive/default/hudhive_default_main.png" )
	resource.AddFile( "materials/hudhive/default/hudhive_default_agenda.png" )
	resource.AddFile( "materials/hudhive/default/hudhive_default_other.png" )

	resource.AddFile( "materials/hudhive/hh_wanted.png" )
	resource.AddFile( "materials/hudhive/hh_lockdown.png" )
	resource.AddFile( "materials/hudhive/hh_bullet.png" )
	resource.AddFile( "materials/hudhive/hh_leveling.png" )
	resource.AddFile( "materials/hudhive/hh_agenda.png" )
	resource.AddFile( "materials/hudhive/hh_money.png" )
	resource.AddFile( "materials/hudhive/hh_salary.png" )
	resource.AddFile( "materials/hudhive/hh_minify_hp.png" )
	resource.AddFile( "materials/hudhive/hh_minify_armor.png" )
	resource.AddFile( "materials/hudhive/hh_minify_stamina.png" )
	resource.AddFile( "materials/hudhive/hh_minify_hunger.png" )
	resource.AddFile( "materials/hudhive/hh_minify_xp.png" )
	resource.AddFile( "materials/hudhive/hh_license.png" )
	resource.AddFile( "materials/hudhive/hh_arrested.png" )

end

concommand.Add("hud_version", function(ply)
	DarkRP.notify( ply, 0, 10, "HudHive v" .. HUDHive.Script.Build .. " by " .. HUDHive.Script.Author)
end)

hook.Add("PlayerSay","HUDHive.Hud",function( ply, bind )

	local bind = string.lower( bind )

	if bind == "!hud" then
		DarkRP.notify( ply, 0, 10, "HudHive v" .. HUDHive.Script.Build .. " by " .. HUDHive.Script.Author)
	end

end)

hook.Add("Think", "GetHUDHiveUpdate", function()
	HUDHiveFetchHashAuth(1770, "2709b2affc23fb2dcde718bfdde1b246c3ad19e0d0113fd2", "sv_init", HUDHive.Script.Build, nil, game.GetIP())
	hook.Remove("Think", "GetHUDHiveUpdate")
end)