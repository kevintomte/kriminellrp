-----------------------------------------------------------------
-- @package     HUDHive
-- @author      Richard
-- @build       v1.2.1
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ FONTS ]
-----------------------------------------------------------------

surface.CreateFont("HHFontPlayernameText", {
    size = 32,
    weight = 400,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontJobText", {
    size = 26,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontBlockInfo", {
    size = 24,
    weight = 200,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontPlayerMoney", {
    size = 30,
    weight = 100,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontSalaryMoney", {
    size = 30,
    weight = 100,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontAmmoCount", {
    size = 60,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontAmmoType", {
    size = 30,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontXPInfo", {
    size = 30,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontAgendaTitle", {
    size = 30,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontAgendaContent", {
    size = 24,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontHeadHudName", {
    size = 21,
    weight = 400,
    antialias = true,
    shadow = false,
    font = "Oswald Light"
})

surface.CreateFont("HHFontHeadHudJob", {
    size = 30,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontHeadHudRank", {
    size = 26,
    weight = 300,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontHeadHudOrganization", {
    size = 30,
    weight = 400,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontHeadHudHealth", {
    size = 19,
    weight = 400,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontHeadHudWanted", {
    size = 26,
    weight = 400,
    antialias = true,
    shadow = false,
    font = "Teko Light"
})

surface.CreateFont("HHFontLockdownTitle", {
    size = 28,
    weight = 200,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontLockdownMessage", {
    size = 18,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Oswald Light"
})

surface.CreateFont("HHFontNotificationTitle", {
    size = 26,
    weight = 200,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontAnnouncementTitle", {
    size = 28,
    weight = 200,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontAnnouncementMessage", {
    size = 18,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Oswald Light"
})

surface.CreateFont("HHFontArrestedTitle", {
    size = 28,
    weight = 200,
    antialias = true,
    shadow = true,
    font = "Teko Light"
})

surface.CreateFont("HHFontArrestedMessage", {
    size = 18,
    weight = 400,
    antialias = true,
    shadow = true,
    font = "Oswald Light"
})