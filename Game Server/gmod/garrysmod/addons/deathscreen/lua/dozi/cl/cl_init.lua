-----------------------------------------------------------------
-- @package		Dozi
-- @author		Richard
-- @build 		v1.3
-- @release		09.19.2015
-----------------------------------------------------------------

Dozi = Dozi or {}

-----------------------------------------------------------------
-- GLOBALS
-----------------------------------------------------------------

DOZI_FetchAlpha = DOZI_FetchAlpha or 0 
DOZI_FadeReverseSet = DOZI_FadeReverseSet or 0
DOZI_FadeoutTimerSet = DOZI_FadeoutTimerSet or 0
DOZI_EffectFade = DOZI_EffectFade or 0

-----------------------------------------------------------------
-- MATERIALS
-----------------------------------------------------------------

local DoziImageKilled = "dozi/dozi_gui_killed.png"
local DoziImageHand = "dozi/dozi_item_hand.png"
local DoziImageKills = "dozi/dozi_gui_kills.png"
local DoziImageDeaths = "dozi/dozi_gui_deaths.png"
local blur = Material("pp/blurscreen")

-----------------------------------------------------------------
-- FONTS
-----------------------------------------------------------------

surface.CreateFont( "DoziDeathPrenote", 
{
	size = 18,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathNotice", 
{
	size = 28,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathCause", 
{
	size = 28,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathTimerText", 
{
	size = 20,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathTimer", 
{
	size = 44,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathTimerReady", 
{
	size = 30,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathGTAWasted", 
{
	size = 72,
	weight = 700,
	antialias = true,
	shadow = false,
	outline = true,
	font = "Pricedown Bl"
})

surface.CreateFont( "DoziDeathGTADeathCaused", 
{
	size = 42,
	weight = 300,
	antialias = true,
	shadow = false,
	outline = true,
	font = "Pricedown Bl"
})

surface.CreateFont( "DoziDeathGTARespawn", 
{
	size = 52,
	weight = 200,
	antialias = true,
	shadow = false,
	outline = true,
	font = "Pricedown Bl"
})

surface.CreateFont( "DoziDeathREDied", 
{
	size = 76,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathREContinue", 
{
	size = 30,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathRETimer", 
{
	size = 72,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathREPrenote", 
{
	size = 46,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Marlett"
})

surface.CreateFont( "DoziDeathREDiedCF", 
{
	size = 104,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Amburegul"
})

surface.CreateFont( "DoziDeathRETimerCF", 
{
	size = 200,
	weight = 700,
	antialias = true,
	shadow = false,
	font = "Dashley"
})

surface.CreateFont( "DoziDeathREPrenoteCF", 
{
	size = 150,
	weight = 200,
	antialias = true,
	shadow = false,
	font = "Amburegul"
})

surface.CreateFont( "DoziDeathREContinueCF", 
{
	size = 80,
	weight = 300,
	antialias = true,
	shadow = false,
	font = "Amburegul"
})



-----------------------------------------------------------------
-- BLUR EFFECT
-----------------------------------------------------------------

function drawBlurAt( x, y, w, h, amount, passes, reverse )
    amount = amount or 5
    surface.SetMaterial(blur)
    surface.SetDrawColor(255, 255, 255)
    local scrW, scrH = ScrW(), ScrH()
    local x2, y2 = x / scrW, y / scrH
    local w2, h2 = (x + w) / scrW, (y + h) / scrH
    for i = -(passes or 0.2), 1, 0.2 do
    	if reverse then
    		blur:SetFloat("$blur", i*-1 * amount)
    	else
    		blur:SetFloat("$blur", i * amount)
    	end
    	blur:Recompute()
    	render.UpdateScreenEffectTexture()
    	surface.DrawTexturedRectUV(x, y, w, h, x2, y2, w2, h2)
    end
end

-----------------------------------------------------------------
-- NET RECEIVERS
-----------------------------------------------------------------

local time = 0
net.Receive("DoziDetectDeath", function(len)
	local time = net.ReadFloat()
	if !time or time == nil then
		respawnTimer = 0
	else
		respawnTimer = time
	end

	if Dozi.Settings.StyleDozi and Dozi.Settings.SoundDoziEnabled then
		surface.PlaySound( Dozi.Settings.SoundDoziPath )
	elseif Dozi.Settings.StyleResidentEvil and Dozi.Settings.SoundREEnabled then
		surface.PlaySound( Dozi.Settings.SoundREPath )
	elseif Dozi.Settings.StyleGTA and Dozi.Settings.SoundGTAEnabled then
		surface.PlaySound( Dozi.Settings.SoundGTAPath )
	end


end)

net.Receive("DoziFetchDeathInfo", function(len)
	local k = net.ReadEntity()
	DoziFetchEntity = net.ReadString()
	doziFetchNick = net.ReadString()
	doziGetKiller = k
	doziKillerNick = n
end)

net.Receive("DoziMessageSet", function( len ) 
	local msg = net.ReadTable()
	chat.AddText( unpack( msg ) )
end)

-----------------------------------------------------------------
-- Effects
-----------------------------------------------------------------

local effectsListBlood = {
	Material("dozi/dozi_blood_1.png"),
	Material("dozi/dozi_blood_2.png"),
	Material("dozi/dozi_blood_3.png")
}

local effectsListGlass = {
	Material("dozi/dozi_glass_1.png"),
	Material("dozi/dozi_glass_2.png")
}

local tableEffectsGlass = {}
local function DoziScreenEffectsGlass()

	local x, y = math.random( 0, ScrW() ), math.random( 0, ScrH() )
	local sxy = math.random( 64, 100 )
	local deg = math.random( 0, 360 )
	local mat = effectsListGlass[ math.random(1, 2) ]

	if table.Count( tableEffectsGlass ) >= Dozi.Settings.GlassAmount then return 
	else
		table.insert( tableEffectsGlass, { xpos = x, ypos = y, sizexy = sxy, deg = deg, mat = mat } )
	end

end

local tableEffectsBlood = {}
local function DoziScreenEffectsBlood()

	local x, y = math.random( 0, ScrW() ), math.random( 0, ScrH() )
	local sxy = math.random( 128, 256 )
	local deg = math.random( 0, 360 )
	local mat = effectsListBlood[ math.random(1, 3) ]

	if table.Count( tableEffectsBlood ) >= Dozi.Settings.BloodAmount then return 
	else
		table.insert( tableEffectsBlood, { xpos = x, ypos = y, sizexy = sxy, deg = deg, mat = mat } )
	end

end

-----------------------------------------------------------------
-- Dozi:DeathHudPaint
-----------------------------------------------------------------

function Dozi:DeathHudPaint()
	if !LocalPlayer():Alive() then

		local FadeIngoal = 230
		local FadeOutgoal = 0

		if DOZI_FadeReverseSet == 0 then
			DOZI_FetchAlpha = math.Approach( DOZI_FetchAlpha, FadeIngoal, FrameTime() * 80 )
		elseif Dozi.Fadeout and DOZI_FadeReverseSet == 1 then
			if DOZI_FadeoutTimerSet == 0 then
				if Dozi.TimerEnable and Dozi.Timer > 0 then
					startFadeout = CurTime() + Dozi.Timer
				else
					startFadeout = CurTime() + Dozi.FadeoutTimer
				end
				DOZI_FadeoutTimerSet = 1
			end
			if CurTime() > startFadeout then	
				DOZI_FetchAlpha = math.Approach( DOZI_FetchAlpha, FadeOutgoal, FrameTime() * 80 )
				DOZI_EffectFade = 1
			end
		end

		if DOZI_FetchAlpha >= 220 then
			DOZI_FadeReverseSet = 1
		end

		local h = ScrH()
		
		if ( DOZI_FetchAlpha > 0 ) then

			--------------------------------/
			-- Style: Dozi
			--------------------------------/

			if Dozi.Settings.StyleDozi then

				-- Top Panel
				-- drawBlurAt( 0, 0 , ScrW(), 85, 6)
				draw.RoundedBox( 0, 0,0, ScrW(), 85, Color( 0, 0, 0, DOZI_FetchAlpha ) )
				surface.SetDrawColor( Dozi.Settings.DoziIconKilledColorR, Dozi.Settings.DoziIconKilledColorG, Dozi.Settings.DoziIconKilledColorB, DOZI_FetchAlpha ) 
				surface.SetMaterial( Material( DoziImageKilled ) )
				surface.DrawTexturedRect( 10, 10, 64, 64 ) 

				-- Bottom Panel
				-- drawBlurAt( 0, ScrH() - 135 , ScrW(), 135, 3 )
				draw.RoundedBox( 0, 0, ScrH() - 135, ScrW(), 135, Color( 0, 0, 0, DOZI_FetchAlpha ) )

				-- Kill/Death Counter (Bottom Left)
				surface.SetDrawColor( Dozi.Settings.DoziIconKDColorR, Dozi.Settings.DoziIconKDColorG, Dozi.Settings.DoziIconKDColorB, DOZI_FetchAlpha )
				surface.SetMaterial( Material ( DoziImageKills ) )
				surface.DrawTexturedRect ( 35, ScrH() - 110, 32, 32 )

				surface.SetDrawColor( Dozi.Settings.DoziIconKDColorR, Dozi.Settings.DoziIconKDColorG, Dozi.Settings.DoziIconKDColorB, DOZI_FetchAlpha )
				surface.SetMaterial( Material ( DoziImageDeaths ) )
				surface.DrawTexturedRect ( 35, ScrH() - 70, 32, 32 )

				draw.SimpleText( LocalPlayer():Frags() , "DoziDeathTimerText", 80, ScrH() - 95, Color( Dozi.Settings.DoziTextKDColorR, Dozi.Settings.DoziTextKDColorG, Dozi.Settings.DoziTextKDColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
				draw.SimpleText( LocalPlayer():Deaths() , "DoziDeathTimerText", 80, ScrH() - 55, Color( Dozi.Settings.DoziTextKDColorR, Dozi.Settings.DoziTextKDColorG, Dozi.Settings.DoziTextKDColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)

			end

			--------------------------------/
			-- Style: Grand Theft Auto 
			--------------------------------/

			if Dozi.Settings.StyleGTA then

				local Gradient = surface.GetTextureID("gui/gradient_up")
				surface.SetDrawColor( 0, 0, 0, 255 );
				surface.SetTexture( Gradient );
				surface.DrawTexturedRect( 0, ScrH()-400, ScrW(), 400);

			end

			--------------------------------/
			-- Style: Resident Evil
			--------------------------------/

			if Dozi.Settings.StyleResidentEvil then
				draw.RoundedBox( 0, 0, ScrH() / 2 - 70, ScrW(), 150, Color( 0, 0, 0, DOZI_FetchAlpha ) )
				surface.SetDrawColor( 255, 0, 0, DOZI_FetchAlpha ) 
				surface.SetMaterial( Material( DoziImageHand ) )
				surface.DrawTexturedRect( ScrH() / 2 + 32, ScrH() / 2 - 256, 512, 512 ) 
			end

			--------------------------------/
			-- Style: Dozi
			--------------------------------/

			if Dozi.TimerEnable and respawnTimer then

				calcTime = math.Round( respawnTimer - CurTime() )

				if calcTime <= 0 then

					if Dozi.Settings.StyleDozi then

						if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
							draw.SimpleText( Dozi.Language["spectator"], "DoziDeathTimerReady", (ScrW() / 2), (ScrH() / 2) + 305, Color( Dozi.Settings.DoziTextRespawnColorR, Dozi.Settings.DoziTextRespawnColorG, Dozi.Settings.DoziTextRespawnColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						else
							draw.SimpleText( Dozi.Language["respawn"], "DoziDeathTimerReady", (ScrW() / 2), (ScrH() / 2) + 305, Color( Dozi.Settings.DoziTextRespawnColorR, Dozi.Settings.DoziTextRespawnColorG, Dozi.Settings.DoziTextRespawnColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						end

					elseif Dozi.Settings.StyleResidentEvil then

						if Dozi.Settings.RECustomFont then
							if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
								draw.SimpleText( Dozi.Language["spectator"], "DoziDeathREContinueCF", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.RETextContinueColorR, Dozi.Settings.RETextContinueColorG, Dozi.Settings.RETextContinueColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
							else
								draw.SimpleText( Dozi.Language["continue"], "DoziDeathREContinueCF", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.RETextContinueColorR, Dozi.Settings.RETextContinueColorG, Dozi.Settings.RETextContinueColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
							end

						else
							if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
								draw.SimpleText( Dozi.Language["spectator"], "DoziDeathREContinue", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.RETextContinueColorR, Dozi.Settings.RETextContinueColorG, Dozi.Settings.RETextContinueColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
							else
								draw.SimpleText( Dozi.Language["continue"], "DoziDeathREContinue", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.RETextContinueColorR, Dozi.Settings.RETextContinueColorG, Dozi.Settings.RETextContinueColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
							end
						end

					elseif Dozi.Settings.StyleGTA then
						if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
							draw.SimpleText( Dozi.Language["spectator"], "DoziDeathGTARespawn", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.GTATextRespawnColorR, Dozi.Settings.GTATextRespawnColorG, Dozi.Settings.GTATextRespawnColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						else
							draw.SimpleText( Dozi.Language["respawn"], "DoziDeathGTARespawn", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.GTATextRespawnColorR, Dozi.Settings.GTATextRespawnColorG, Dozi.Settings.GTATextRespawnColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						end
					end

				else

					if Dozi.Settings.StyleDozi then

						draw.SimpleText( Dozi.Language["respawn_in"] , "DoziDeathTimerText", (ScrW() / 2), (ScrH() / 2) + 285, Color( Dozi.Settings.DoziTextRespawnInColorR, Dozi.Settings.DoziTextRespawnInColorG, Dozi.Settings.DoziTextRespawnInColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						draw.SimpleText( calcTime , "DoziDeathTimer", (ScrW() / 2), (ScrH() / 2) + 320, Color( Dozi.Settings.DoziTextTimerColorR, Dozi.Settings.DoziTextTimerColorG, Dozi.Settings.DoziTextTimerColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

					elseif Dozi.Settings.StyleResidentEvil then

						if Dozi.Settings.RECustomFont then
							draw.SimpleText( calcTime , "DoziDeathRETimerCF", (ScrW() / 2), (ScrH() / 2) + 160, Color( Dozi.Settings.RETextTimerColorR, Dozi.Settings.RETextTimerColorG, Dozi.Settings.RETextTimerColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						else
							draw.SimpleText( calcTime , "DoziDeathRETimer", (ScrW() / 2), (ScrH() / 2) + 160, Color( Dozi.Settings.RETextTimerColorR, Dozi.Settings.RETextTimerColorG, Dozi.Settings.RETextTimerColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
						end

					elseif Dozi.Settings.StyleGTA then

						draw.SimpleText( calcTime , "DoziDeathGTAWasted", (ScrW() / 2), (ScrH() / 2) + 300, Color( Dozi.Settings.GTATextTimerColorR, Dozi.Settings.GTATextTimerColorG, Dozi.Settings.GTATextTimerColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

					end

				end

			end

			------------------------------------
			-- If player is killed by yourself
			------------------------------------

			if doziGetKiller and doziGetKiller == LocalPlayer() then

				--------------------------------/
				-- Style: Dozi
				-- Suicide Message
				--------------------------------/

				if Dozi.Settings.StyleDozi then
					draw.SimpleText( Dozi.Language["you_are_dead"], "DoziDeathCause", 90, 30, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )

				--------------------------------/
				-- Style: Resident Evil
				-- Suicide Message
				--------------------------------/

				elseif Dozi.Settings.StyleResidentEvil then

					if Dozi.Settings.RECustomFont then
						draw.SimpleText( Dozi.Language["you_are_dead"], "DoziDeathREPrenoteCF", ScrW() / 2, ScrH() / 2 + 20, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					else
						draw.SimpleText( Dozi.Language["you_are_dead"], "DoziDeathREDied", ScrW() / 2, ScrH() / 2, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					end

				elseif Dozi.Settings.StyleGTA then

					draw.SimpleText( Dozi.Language["gta_wasted"], "DoziDeathGTAWasted", ScrW() / 2, ScrH() / 2, Color( Dozi.Settings.GTATextWastedColorR, Dozi.Settings.GTATextWastedColorG, Dozi.Settings.GTATextWastedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

				end

			else

				--------------------------------/
				-- Format Entity from Table
				--------------------------------/

				tableEntry = DoziListOther[DoziFetchEntity]
				if tableEntry == nil then
					returnEntry = DoziListOther["other"]
				else
					returnEntry = tableEntry
				end

				--------------------------------/
				-- If the killer is an actual
				-- player.
				--------------------------------/

				if doziGetKiller and doziGetKiller:IsPlayer() then

					--------------------------------/
					-- Style: Dozi
					-- Prenote - You Were Killed By
					--------------------------------/

					if Dozi.Settings.StyleDozi then

						if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
							draw.SimpleText( Dozi.Language["you_are_dead"], "DoziDeathCause", 90, 30, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						else
							draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathPrenote", 90, 20, Color( Dozi.Settings.DoziTextPrenoteColorR, Dozi.Settings.DoziTextPrenoteColorG, Dozi.Settings.DoziTextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
							draw.SimpleText( doziFetchNick, "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						end

					--------------------------------/
					-- Style: Resident Evil
					-- Prenote - You Were Killed By
					--------------------------------/

					elseif Dozi.Settings.StyleResidentEvil then

						if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
							if Dozi.Settings.RECustomFont then
								draw.SimpleText( Dozi.Language["you_are_dead"], "DoziDeathREPrenoteCF", ScrW() / 2, ScrH() / 2 + 20, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							else
								draw.SimpleText( Dozi.Language["you_are_dead"], "DoziDeathREDied", ScrW() / 2, ScrH() / 2, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							end
						else
							if Dozi.Settings.RECustomFont then
								draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathREDiedCF", ScrW() / 2, ScrH() / 2, Color( Dozi.Settings.RETextDiedColorR, Dozi.Settings.RETextDiedColorG, Dozi.Settings.RETextDiedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							else
								draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathREDied", ScrW() / 2, ScrH() / 2, Color( Dozi.Settings.RETextDiedColorR, Dozi.Settings.RETextDiedColorG, Dozi.Settings.RETextDiedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							end
							draw.SimpleText( doziFetchNick, "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end


					elseif Dozi.Settings.StyleGTA then

						if gmod.GetGamemode().Name == "Trouble in Terrorist Town" then
							draw.SimpleText( Dozi.Language["gta_wasted"], "DoziDeathGTAWasted", ScrW() / 2, ScrH() / 2, Color( Dozi.Settings.GTATextWastedColorR, Dozi.Settings.GTATextWastedColorG, Dozi.Settings.GTATextWastedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						else
							if Dozi.Settings.GTASeparatorShow then
								surface.SetDrawColor( Color( Dozi.Settings.GTASeperatorColorR, Dozi.Settings.GTASeperatorColorG, Dozi.Settings.GTASeperatorColorB, DOZI_FetchAlpha - 150 ) )
								surface.DrawRect( ScrW()/2-200, ScrH()/2+22, ScrW()/2-275, 1 )
							end

							draw.SimpleText( Dozi.Language["gta_wasted"], "DoziDeathGTAWasted", ScrW() / 2, ScrH() / 2 - 20, Color( Dozi.Settings.GTATextWastedColorR, Dozi.Settings.GTATextWastedColorG, Dozi.Settings.GTATextWastedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							draw.SimpleText( doziFetchNick, "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end

					end

				--------------------------------/
				-- If death caused by NPC
				--------------------------------/

				elseif doziGetKiller and doziGetKiller:IsNPC() then

					--------------------------------/
					-- Style: Dozi
					-- If killed by NPC
					--------------------------------/

					if Dozi.Settings.StyleDozi then

						draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathPrenote", 90, 20, Color( Dozi.Settings.DoziTextPrenoteColorR, Dozi.Settings.DoziTextPrenoteColorG, Dozi.Settings.DoziTextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )

						if DoziListNPC[DoziFetchEntity] != nil then
							draw.SimpleText( DoziListNPC[DoziFetchEntity], "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						else
							draw.SimpleText( DoziFetchEntity or "Other", "DoziDeathCause", (ScrW()/2) - 25, (ScrH()/2) + 53, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						end

					--------------------------------/
					-- Style: Resident Evil
					-- If killed by NPC
					--------------------------------/

					elseif Dozi.Settings.StyleResidentEvil then

						draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathREPrenoteCF", ScrW() / 2, ScrH() / 2 - 10, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

						if DoziListNPC[DoziFetchEntity] != nil then
							draw.SimpleText( DoziListNPC[DoziFetchEntity], "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						else
							draw.SimpleText( DoziFetchEntity or "Other", "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end

					--------------------------------/
					-- Style: GTA
					-- If killed by NPC
					--------------------------------/

					elseif Dozi.Settings.StyleGTA then

						draw.SimpleText( Dozi.Language["gta_wasted"], "DoziDeathGTAWasted", ScrW() / 2, ScrH() / 2 - 20, Color( Dozi.Settings.GTATextWastedColorR, Dozi.Settings.GTATextWastedColorG, Dozi.Settings.GTATextWastedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

						if Dozi.Settings.GTASeparatorShow then
							surface.SetDrawColor( Dozi.Settings.GTASeperatorColorR, Dozi.Settings.GTASeperatorColorG, Dozi.Settings.GTASeperatorColorB, DOZI_FetchAlpha - 150 )
							surface.DrawRect( ScrW()/2-200, ScrH()/2+22, ScrW()/2-275, 1 )
						end

						if DoziListNPC[DoziFetchEntity] != nil then
							draw.SimpleText( DoziListNPC[DoziFetchEntity], "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						else
							draw.SimpleText( DoziFetchEntity or "Other", "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end

					end

				else

					--------------------------------/
					-- Style: Dozi
					-- Enities
					--------------------------------/

					if Dozi.Settings.StyleDozi then

						draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathPrenote", 90, 20, Color( Dozi.Settings.DoziTextPrenoteColorR, Dozi.Settings.DoziTextPrenoteColorG, Dozi.Settings.DoziTextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )

						if DoziFetchEntity == "prop_physics" then
							draw.SimpleText( DoziListWorld["prop_physics"], "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						elseif DoziFetchEntity == "world" then
							draw.SimpleText( DoziListWorld["world"], "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						elseif DoziFetchEntity == "gravity" then
							draw.SimpleText( DoziListWorld["gravity"], "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						elseif DoziFetchEntity == "worldspawn" then
							draw.SimpleText( DoziListWorld["worldspawn"], "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						elseif DoziFetchEntity == "prop_vehicle_jeep" or DoziFetchEntity == "prop_vehicle_airboat" then
							draw.SimpleText( DoziListVehicle[DoziFetchEntity], "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						else
							draw.SimpleText( returnEntry, "DoziDeathCause", 90, 40, Color( Dozi.Settings.DoziTextCauseColorR, Dozi.Settings.DoziTextCauseColorG, Dozi.Settings.DoziTextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT )
						end

					--------------------------------/
					-- Style: Resident Evil
					-- Enities
					--------------------------------/

					elseif Dozi.Settings.StyleResidentEvil then

						if Dozi.Settings.RECustomFont then
							draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathREPrenoteCF", ScrW() / 2, ScrH() / 2 - 10, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						else
							draw.SimpleText( Dozi.Language["you_were_killed_by"], "DoziDeathREPrenote", ScrW() / 2, ScrH() / 2 - 20, Color( Dozi.Settings.RETextPrenoteColorR, Dozi.Settings.RETextPrenoteColorG, Dozi.Settings.RETextPrenoteColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end

						if DoziFetchEntity == "prop_physics" then
							draw.SimpleText( DoziListWorld["prop_physics"], "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "world" then
							draw.SimpleText( DoziListWorld["world"], "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "gravity" then
							draw.SimpleText( DoziListWorld["gravity"], "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "worldspawn" then
							draw.SimpleText( DoziListWorld["worldspawn"], "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "prop_vehicle_jeep" or DoziFetchEntity == "prop_vehicle_airboat" then
							draw.SimpleText( DoziListVehicle[DoziFetchEntity], "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						else
							draw.SimpleText( returnEntry, "DoziDeathCause", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.RETextCauseColorR, Dozi.Settings.RETextCauseColorG, Dozi.Settings.RETextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end

					--------------------------------/
					-- Style: GTA
					-- Enities
					--------------------------------/

					elseif Dozi.Settings.StyleGTA then

						draw.SimpleText( Dozi.Language["gta_wasted"], "DoziDeathGTAWasted", ScrW() / 2, ScrH() / 2 - 20, Color( Dozi.Settings.GTATextWastedColorR, Dozi.Settings.GTATextWastedColorG, Dozi.Settings.GTATextWastedColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

						if Dozi.Settings.GTASeparatorShow then
							surface.SetDrawColor( Dozi.Settings.GTASeperatorColorR, Dozi.Settings.GTASeperatorColorG, Dozi.Settings.GTASeperatorColorB, DOZI_FetchAlpha - 150 )
							surface.DrawRect( ScrW()/2-200, ScrH()/2+22, ScrW()/2-275, 1 )
						end

						if DoziFetchEntity == "prop_physics" then
							draw.SimpleText( DoziListWorld["prop_physics"], "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "world" then
							draw.SimpleText( DoziListWorld["world"], "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "gravity" then
							draw.SimpleText( DoziListWorld["gravity"], "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "worldspawn" then
							draw.SimpleText( DoziListWorld["worldspawn"], "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						elseif DoziFetchEntity == "prop_vehicle_jeep" or DoziFetchEntity == "prop_vehicle_airboat" then
							draw.SimpleText( DoziListVehicle[DoziFetchEntity], "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						else
							draw.SimpleText( returnEntry, "DoziDeathGTADeathCaused", ScrW() / 2, ScrH() / 2 + 40, Color( Dozi.Settings.GTATextCauseColorR, Dozi.Settings.GTATextCauseColorG, Dozi.Settings.GTATextCauseColorB, DOZI_FetchAlpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
						end

					end

				end

			end

			--------------------------------/
			-- Effect: Glass
			--------------------------------/

			if Dozi.Settings.GlassEnable then

				DoziScreenEffectsGlass()

				for i = #tableEffectsGlass, 1, -1 do
					surface.SetDrawColor( 255, 255, 255, DOZI_FetchAlpha )
					surface.SetMaterial( tableEffectsGlass[i].mat )
					surface.DrawTexturedRectRotated( tableEffectsGlass[i].xpos, tableEffectsGlass[i].ypos, tableEffectsGlass[i].sizexy, tableEffectsGlass[i].sizexy, tableEffectsGlass[i].deg ) 
				end

			end

			--------------------------------/
			-- Effect: Blood
			--------------------------------/

			if Dozi.Settings.BloodEnable then

				DoziScreenEffectsBlood()

				for i = #tableEffectsBlood, 1, -1 do
					surface.SetDrawColor( 255, 255, 255, DOZI_FetchAlpha )
					surface.SetMaterial( tableEffectsBlood[i].mat )
					surface.DrawTexturedRectRotated( tableEffectsBlood[i].xpos, tableEffectsBlood[i].ypos, tableEffectsBlood[i].sizexy, tableEffectsBlood[i].sizexy, tableEffectsBlood[i].deg ) 
				end

			end


		end
	
	else

		--------------------------------/
		-- Reset Values
		--------------------------------/

		DOZI_FetchAlpha = 0 
		DoziGreyModify = 1	
		DoziFadeModify = 1
		doziKillerNick = ""
		DOZI_FadeReverseSet = 0
		DOZI_EffectFade = 0
		DOZI_FadeoutTimerSet = 0
		table.Empty( tableEffectsBlood ) 
		table.Empty( tableEffectsGlass )
		returnEntry = ""

	end
	
end
hook.Add("HUDPaint", "DoziDeathHudPaint", Dozi.DeathHudPaint)

-----------------------------------------------------------------
-- Dozi:RenderEffects
-----------------------------------------------------------------

function Dozi:RenderEffects()
	if !LocalPlayer():Alive() then

		if Dozi.Settings.StyleGTA then

			if DOZI_EffectFade == 0 then

				DoziGreyModify = DoziGreyModify or 1
				local DoziGreyGoal = 0
				DoziGreyModify = math.Approach( DoziGreyModify, DoziGreyGoal, FrameTime() * 0.5 )

				local DoziModifyColor = {}
				DoziModifyColor[ "$pp_colour_brightness" ] = 0	-- Default 0
				DoziModifyColor[ "$pp_colour_colour" ] = DoziGreyModify		-- Default 1
				DoziModifyColor[ "$pp_colour_contrast" ] = 1				-- Default 1

				DrawColorModify( DoziModifyColor )

			elseif DOZI_EffectFade == 1 then

				DoziGreyModify = DoziGreyModify or 0
				local DoziGreyGoal = 1
				DoziGreyModify = math.Approach( DoziGreyModify, DoziGreyGoal, FrameTime() * 0.5 )

				local DoziModifyColor = {}
				DoziModifyColor[ "$pp_colour_brightness" ] = 0	-- Default 0
				DoziModifyColor[ "$pp_colour_colour" ] = DoziGreyModify		-- Default 1
				DoziModifyColor[ "$pp_colour_contrast" ] = 1				-- Default 1

				DrawColorModify( DoziModifyColor )

			end


		end

		if Dozi.Settings.StyleResidentEvil then

			if DOZI_EffectFade == 0 then
				DoziGreyGoal = 0.2
			elseif DOZI_EffectFade == 1 then
				DoziGreyGoal = 1
			end

			DoziGreyModify = DoziGreyModify or 1
			DoziGreyModify = math.Approach( DoziGreyModify, DoziGreyGoal, FrameTime() * 0.5 )

			local DoziModifyColor = {}
			DoziModifyColor[ "$pp_colour_brightness" ] = 0				-- Default 0
			DoziModifyColor[ "$pp_colour_colour" ] = DoziGreyModify		-- Default 1
			DoziModifyColor[ "$pp_colour_contrast" ] = DoziGreyModify	-- Default 1

			DrawColorModify( DoziModifyColor )

		end

		if Dozi.Settings.DoziFXGreyscale and !Dozi.Settings.StyleGTA and !Dozi.Settings.StyleResidentEvil then

			if DOZI_EffectFade == 0 then
				DoziGreyGoal = 0
			elseif DOZI_EffectFade == 1 then
				DoziGreyGoal = 1
			end

			DoziGreyModify = DoziGreyModify or 0
			DoziGreyModify = math.Approach( DoziGreyModify, DoziGreyGoal, FrameTime() * 0.5 )

			local DoziModifyColor = {}
			DoziModifyColor[ "$pp_colour_brightness" ] = 0				-- Default 0
			DoziModifyColor[ "$pp_colour_colour" ] = DoziGreyModify		-- Default 1
			DoziModifyColor[ "$pp_colour_contrast" ] = 1				-- Default 1

			DrawColorModify( DoziModifyColor )

		end

		if Dozi.Settings.DoziFXFade and !Dozi.Settings.StyleGTA and !Dozi.Settings.StyleResidentEvil then

			if DOZI_EffectFade == 0 then
				DoziFadeGoal = 0.3
			elseif DOZI_EffectFade == 1 then
				DoziFadeGoal = 1
			end

			DoziFadeModify = DoziFadeModify or 1
			DoziFadeModify = math.Approach( DoziFadeModify, DoziFadeGoal, FrameTime() * 0.5 )

			local DoziModifyColor = {}
			DoziModifyColor[ "$pp_colour_brightness" ] = 0				-- Default 0
			DoziModifyColor[ "$pp_colour_colour" ] = 1					-- Default 1
			DoziModifyColor[ "$pp_colour_contrast" ] = DoziFadeModify	-- Default 1

			DrawColorModify( DoziModifyColor )

		end

		if Dozi.Settings.DoziFXSobel and !Dozi.Settings.StyleGTA and !Dozi.Settings.StyleResidentEvil then

			if DOZI_EffectFade == 0 then
				DoziSobelModify = DoziSobelModify or 1 
				DoziSobelGoal = 0.8
			elseif DOZI_EffectFade == 1 then
				DoziSobelModify = DoziSobelModify or 0.8 
				DoziSobelGoal = 3
			end

			DoziSobelModify = math.Approach( DoziSobelModify, DoziSobelGoal, FrameTime() * 0.5 )

			DrawSobel( DoziSobelModify )
			print (DoziSobelModify)
		end

		if Dozi.Settings.DoziFXSharpen and !Dozi.Settings.StyleGTA and !Dozi.Settings.StyleResidentEvil then

			if DOZI_EffectFade == 0 then
				DrawSharpen( 1.000, 1 )
			elseif DOZI_EffectFade == 1 then
				DrawSharpen( 0.000, 1 )
			end
		end

	end
end
hook.Add("RenderScreenspaceEffects", "DoziRenderEffects", Dozi.RenderEffects)

function Dozi:HideGUI( obj )

	if ( obj == "CHudDamageIndicator" ) or ( obj == "CHudHealth" ) or ( obj == "CHudBattery" ) then
		if !LocalPlayer():Alive( ) then
			return false
		end
	end

end
hook.Add( "HUDShouldDraw", "Dozi.HideGUI", Dozi.HideGUI )
