--Pointshop Betting System Fonts
local function LoadPointshopBettingFonts()
if BETTING.FontsLoaded then return end
surface.CreateFont("Bebas30Font", {font = "Bebas Neue", size= 30, weight = 400, antialias = true } )
surface.CreateFont("Bebas40Font", {font = "Bebas Neue", size= 40, weight = 400, antialias = true } )
surface.CreateFont("Bebas70Font", {font = "Bebas Neue", size= 70, weight = 400, antialias = true } )
surface.CreateFont("Bebas120Font", {font = "Bebas Neue", size= 120, weight = 400, antialias = true } )

surface.CreateFont("OpenSans35Font", {font = "Open Sans Condensed", size= 35, weight = 400, antialias = true } )
surface.CreateFont("OpenSans24Font", {font = "Open Sans Condensed", size= 24, weight = 400, antialias = true } )
surface.CreateFont("OpenSans18Font", {font = "Open Sans Condensed", size= 18, weight = 400, antialias = true } )
surface.CreateFont("OpenSans20Font", {font = "Open Sans Condensed", size= 20, weight = 400, antialias = true } )
BETTING.FontsLoaded = true
end
LoadPointshopBettingFonts()
hook.Add("InitPostEntity", "LoadPointshopBettingFonts", LoadPointshopBettingFonts)