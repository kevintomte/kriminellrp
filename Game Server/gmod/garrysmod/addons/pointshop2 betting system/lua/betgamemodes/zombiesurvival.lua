--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for the Zombie Survival gamemode by JetBoom

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_UNDEAD
BETTING.SecondTeam = TEAM_HUMAN
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5

local function PlayerCanZS(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if GetGlobalInt("wave", 0) != 1 then return false,"You can only bet once the first wave has began." end
		local wavestart = GetGlobalFloat("wavestart", gmod.GetGamemode().WaveZeroLength)
		if (wavestart + BETTING.Settings.OnlyAllowBettingAtRoundStartTime) < CurTime() then
			return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
		end
		return true
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Team() != TEAM_UNDEAD then return false,"Only the undead can place a bet!" end
	//2 is round active, why aren't the round enums shared?
	if GetGlobalInt("wave", 0) != 1 then return false ,"You can only bet during the first wave." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanZS",PlayerCanZS)

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
local function ZSEndRoundBets(winner)
	if not winner then return end
	if (winner != TEAM_UNDEAD) and(winner != TEAM_HUMAN) then BETTING.FinishBets(winner, true)
	else BETTING.FinishBets(winner) end
end
hook.Add("EndRound","ZSEndRoundBets",ZSEndRoundBets)
end