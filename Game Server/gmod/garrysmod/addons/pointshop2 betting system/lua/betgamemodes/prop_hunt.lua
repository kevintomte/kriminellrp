--Pointshop betting gamemode scripts are SHARED. Hooks must return the same data on both server and client.
--This script is for Kowalski's prop hunt gamemode.

--Gamemode scripts MUST ALWAYS specify the first and second teams to bet on with these settings:
BETTING.FirstTeam = TEAM_PROPS
BETTING.SecondTeam = TEAM_HUNTERS
//You could use this file to load custom settings based on the gamemode e.g.
//BETTING.Settings.MinimumPlayersForBetting = 5
local function PlayerCanBetPropHunt(ply)
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime enabled
	if tonumber(BETTING.Settings.OnlyAllowBettingAtRoundStartTime) and BETTING.Settings.OnlyAllowBettingAtRoundStartTime > 0 then
		if BETTING.EndBetsTime and CurTime() <= BETTING.EndBetsTime then return true end
		return false,string.format("You must place a bet in the first %i seconds of the round!",BETTING.Settings.OnlyAllowBettingAtRoundStartTime)
	end
	
	--BETTING.Settings.OnlyAllowBettingAtRoundStartTime disabled
	if ply:Alive() then return false,"Players must be dead or spectating to bet." end
	if !GetGlobalBool("InRound") then return false,"You can't bet whilst a new round is preparing." end
	return true
end
hook.Add("PlayerCanBet","PlayerCanBetPropHunt",PlayerCanBetPropHunt)

local OldOnRoundStart = gmod.GetGamemode().OnRoundStart
local function AllowBetsWhenRoundBegins(gm)
	if SERVER then OldOnRoundStart(gm) end
	if SERVER then SendUserMessage("PSBettingOnStartRound", player.GetAll()) end
	BETTING.EndBetsTime = CurTime() + BETTING.Settings.OnlyAllowBettingAtRoundStartTime or 30
end
if CLIENT then usermessage.Hook("PSBettingOnStartRound",AllowBetsWhenRoundBegins) end
gmod.GetGamemode().OnRoundStart = AllowBetsWhenRoundBegins

--SERVER hooks
--Every outcome must call BETTING.FinishBets else the bet window will not be closed
if SERVER then
	local OldOnRoundEnd = gmod.GetGamemode().RoundEndWithResult
	local function PropHuntEndRoundBets(gm,winner)
		OldOnRoundEnd(gm,winner)
		if not winner then return end
		if winner != TEAM_PROPS and winner != TEAM_HUNTERS then BETTING.FinishBets(winner, true)
		else BETTING.FinishBets(winner) end
	end
	gmod.GetGamemode().RoundEndWithResult = PropHuntEndRoundBets
end