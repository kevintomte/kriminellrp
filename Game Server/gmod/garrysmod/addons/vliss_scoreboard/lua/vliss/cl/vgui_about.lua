-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.3.0
-- @release     12.03.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ FONTS ]
-----------------------------------------------------------------

local PANEL = {}

-----------------------------------------------------------------
-- [ ABOUT VLISS ]
-----------------------------------------------------------------
-- Various information related to vliss. I would appreciate 
-- you leaving all of this intact, as it does help support the 
-- developer. However I'm not going to force you to keep it.
-----------------------------------------------------------------

function PANEL:AboutVlissContent()
    local VlissAboutProduct = [[

Vliss - the Garry's Mod solution to Scoreboards.
Developed by Richard

We want Vliss to be the ultimate complete solution for anyone wishing to have a Scoreboard they can use anywhere they want; even for the not-so-popular game-modes that have faded into the background. This is why with each update, we aim towards adding even more support. Along with supporting these modes, we want our scoreboard to give players/admins special features directly available only within that particular game-mode.

Finally, the other objective with Vliss is to ensure vital network information is exchanged with players. This includes networks being able to provide buttons which take players to their website, donation pages, steam workshop collections, and anything else that the network needs to get out there in order to promote what they do.

-- CREDITS --

[ DEVELOPMENT ]
Richard
Phoenixf129

[ IDEAS / SUGGESTIONS ]
Bobby
Billy
boardinwelts
TheNomBomb

[ TESTERS ]
The entire Ache Gaming Studios (AGS) community!
Everyone at ScriptFodder that purchases this script. They all give vital feedback that helps keep the bugs to a minimum and great recommendations on how the script could even be better. So if you're a regular player viewing this about section; the owner of your network helped make what Vliss is today - tell them thank you.
    
]]

    return VlissAboutProduct
end

function PANEL:VlissUpdateStatusOK(UpdateAvail)

    if UpdateAvail then

    local VlissUpdateStatus = [[
An update for Vliss has been detected.
]]

    else

    local VlissUpdateStatus = [[
No updates are available.
]]

    end

    return VlissUpdateStatus
    
end

function PANEL:Init()


    local vliss = self

    local GetLicenseType
    if Vliss:PerformCheck(VlissFetchHashAuth) then
        GetLicenseType = 1
    else
        GetLicenseType = 2
    end

    self.PanelAV = vgui.Create("DPanel", self)
    self.PanelAV:Dock(FILL)
    self.PanelAV:SetVisible(true)
    self.PanelAV.Paint = function(self, w, h) end

    self.PanelAVContainerT = vgui.Create("DPanel", self.PanelAV)
    self.PanelAVContainerT:Dock(TOP)
    self.PanelAVContainerT:DockMargin(0, 10, 0, 0)
    self.PanelAVContainerT:SetTall(35)
    self.PanelAVContainerT.Paint = function(self, w, h) end

    self.PanelAVContainerTL = vgui.Create("DPanel", self.PanelAVContainerT)
    self.PanelAVContainerTL:Dock(LEFT)
    self.PanelAVContainerTL:DockMargin(0, 0, 0, 0)
    self.PanelAVContainerTL:SetWide(300)
    self.PanelAVContainerTL.Paint = function(self, w, h) end

    self.PanelAVContainerTR = vgui.Create("DPanel", self.PanelAVContainerT)
    self.PanelAVContainerTR:Dock(FILL)
    self.PanelAVContainerTR:DockMargin(0, 0, 0, 0)
    self.PanelAVContainerTR:SetTall(30)
    self.PanelAVContainerTR.Paint = function(self, w, h) end

    self.ButtonAVTitle = vgui.Create("DButton", self.PanelAVContainerTL)
    self.ButtonAVTitle:Dock(TOP)
    self.ButtonAVTitle:SetText("")
    self.ButtonAVTitle:DockMargin(5, 10, 5, 5)
    self.ButtonAVTitle:SetTall(20)
    self.ButtonAVTitle.Paint = function(self, w, h)
        draw.SimpleText("About Vliss Scoreboard", "VlissFontNetworkName", 0, h/2, Vliss.Core.NetworkNameColor or Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
    end
    self.ButtonAVTitle.DoClick = function(self, w, h)
        gui.OpenURL( Vliss.Script.Website )
    end

    self.ButtonAVProductHome = vgui.Create("DButton", self.PanelAVContainerT)
    self.ButtonAVProductHome:SetText("")
    self.ButtonAVProductHome:SetColor(Color(255,255,255,255))
    self.ButtonAVProductHome:SetFont("VlissAboutText")
    self.ButtonAVProductHome:SetSize(150, 15)
    self.ButtonAVProductHome:DockMargin(10,0,0,5)
    self.ButtonAVProductHome:Dock(RIGHT)
    self.ButtonAVProductHome:SetTooltip("View the Vliss ScriptFodder Page")
    self.ButtonAVProductHome.Paint = function(self, w, h)

        local buttonNormal = Color(124, 51, 50, 190)
        local buttonHover = Color(124, 51, 50, 240)
        local textNormal = Color(255, 255, 255, 255)
        local textHover = Color(255, 255, 255, 255)

        local color = buttonNormal
        local txtColor = textNormal

        if self:IsHovered() or self:IsDown() then
            color = buttonHover
            txtColor = textHover
        end

        surface.SetDrawColor(color)
        surface.DrawRect(0, 0, w, h)

        surface.SetDrawColor(Color( 255, 255, 255, 100 ))
        surface.DrawOutlinedRect(0, 0, w, h)

        draw.SimpleText(string.upper("Product Home"), "VlissFontMenuItem", 10, self:GetTall() * .46, txtColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)

    end
    self.ButtonAVProductHome.DoClick = function(self, w, h)
        gui.OpenURL( Vliss.Script.Website )
    end

    self.ButtonAVProductDocs = vgui.Create("DButton", self.PanelAVContainerT)
    self.ButtonAVProductDocs:SetText("")
    self.ButtonAVProductDocs:SetColor(Color(255,255,255,255))
    self.ButtonAVProductDocs:SetFont("VlissAboutText")
    self.ButtonAVProductDocs:SetSize(150, 15)
    self.ButtonAVProductDocs:DockMargin(10,0,0,5)
    self.ButtonAVProductDocs:Dock(RIGHT)
    self.ButtonAVProductDocs:SetTooltip("View the Vliss Documentation")
    self.ButtonAVProductDocs.Paint = function(self, w, h)
        local buttonNormal = Color(64, 105, 126, 190)
        local buttonHover = Color(64, 105, 126, 240)
        local textNormal = Color(255, 255, 255, 255)
        local textHover = Color(255, 255, 255, 255)

        local color = buttonNormal
        local txtColor = textNormal

        if self:IsHovered() or self:IsDown() then
            color = buttonHover
            txtColor = textHover
        end

        surface.SetDrawColor(color)
        surface.DrawRect(0, 0, w, h)

        surface.SetDrawColor(Color( 255, 255, 255, 100 ))
        surface.DrawOutlinedRect(0, 0, w, h)

        draw.SimpleText(string.upper("Documentation"), "VlissFontMenuItem", 10, self:GetTall() * .46, txtColor, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)

    end
    self.ButtonAVProductDocs.DoClick = function(self, w, h)
        gui.OpenURL( Vliss.Script.Documentation )
    end

    self.PanelVlissContainer = vgui.Create("DPanel", self.PanelAV)
    self.PanelVlissContainer:Dock(FILL)
    self.PanelVlissContainer:DockMargin(0, 0, 0, 0)
    self.PanelVlissContainer.Paint = function(self, w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color( 15, 15, 15, 200 ) )
        draw.VlissOutlinedBoxThick( 0, 0, w, h, 3, Color( 10, 10, 10, 100 ) )
    end

    self.PanelAVContainerTL = vgui.Create("DPanel", self.PanelVlissContainer)
    self.PanelAVContainerTL:Dock(FILL)
    self.PanelAVContainerTL:DockMargin(0, 0, 0, 0)
    self.PanelAVContainerTL.Paint = function(self, w, h) end

    self.PanelAVContainerTR = vgui.Create("DPanel", self.PanelVlissContainer)
    self.PanelAVContainerTR:Dock(RIGHT)
    self.PanelAVContainerTR:SetWide(200)
    self.PanelAVContainerTR:DockMargin(0, 0, 0, 0)
    self.PanelAVContainerTR.Paint = function(self, w, h)
        draw.RoundedBox(0, 0, 0, w, h, Color( 50, 15, 15, 200 ) )
    end

    local TranslateLicenseToReadable
    if GetLicenseType == 1 then
        TranslateLicenseToReadable = "Paid License"
    elseif GetLicenseType == 2 then
        TranslateLicenseToReadable = "Free License"
    end

    local ScriptData = {
        {

            title = "Script",
            value = Vliss.Script.Name
        },
        {
            title = "Author",
            value = Vliss.Script.Author
        },
        {
            title = "Version",
            value = Vliss.Script.Build
        },
        {
            title = "Build Released",
            value = Vliss.Script.Released
        },
        {
            title = "License Type",
            value = TranslateLicenseToReadable
        }

    }

    for k, v in pairs(ScriptData) do

        self.AboutVlissLabel = vgui.Create("DLabel", self.PanelAVContainerTR)
        self.AboutVlissLabel:Dock(TOP)
        self.AboutVlissLabel:DockMargin(10, 10, 1, 0)
        self.AboutVlissLabel:SetFont("VlissFontAboutSInfo")
        self.AboutVlissLabel:SetTextColor(Vliss.Core.StaffCardNameColor or Color(255, 255, 255, 255))
        self.AboutVlissLabel:SizeToContents()
        self.AboutVlissLabel:SetText(v.title)

        self.AboutVlissValue = vgui.Create("DLabel", self.PanelAVContainerTR)
        self.AboutVlissValue:Dock(TOP)
        self.AboutVlissValue:DockMargin(10, 0, 10, 4)
        self.AboutVlissValue:SetFont("VlissFontAboutSInfo")
        self.AboutVlissValue:SetTextColor(Vliss.Core.StaffCardNameColor or Color(255, 255, 255, 255))
        self.AboutVlissValue:SizeToContents()
        self.AboutVlissValue:SetText(v.value)

    end

    self.DLabelAVVerAvailable = vgui.Create("DTextEntry", self.PanelAVContainerTR)
    self.DLabelAVVerAvailable:Dock(FILL)
    self.DLabelAVVerAvailable:DockMargin(10, 10, 10, 10)
    self.DLabelAVVerAvailable:SetWide(200)
    self.DLabelAVVerAvailable:SetMultiline(true)
    self.DLabelAVVerAvailable:SetDrawBackground(false)
    self.DLabelAVVerAvailable:SetEnabled(true)
    self.DLabelAVVerAvailable:SetVerticalScrollbarEnabled(true)
    self.DLabelAVVerAvailable:SetFont("VlissAboutUpdateResultText")
    self.DLabelAVVerAvailable:SetText("")
    self.DLabelAVVerAvailable:SetTextColor(Color(255, 255, 255, 255))
    self.DLabelAVVerAvailable:SetVisible(false)

    self.TextAVInformation = vgui.Create("DTextEntry", self.PanelAVContainerTL)
    self.TextAVInformation:Dock(FILL)
    self.TextAVInformation:DockMargin(10, 10, 10, 10)
    self.TextAVInformation:SetWide(200)
    self.TextAVInformation:SetMultiline(true)
    self.TextAVInformation:SetDrawBackground(false)
    self.TextAVInformation:SetEnabled(true)
    self.TextAVInformation:SetVerticalScrollbarEnabled(true)
    self.TextAVInformation:SetFont("VlissFontStandardAbout")
    self.TextAVInformation:SetText(self:AboutVlissContent())
    self.TextAVInformation:SetTextColor(Color(255, 255, 255, 255))

    self.ButtonVlissUpdates = vgui.Create("DButton", self.PanelAVContainerTR)
    self.ButtonVlissUpdates:SetText(Vliss.Language.check_for_updates)
    self.ButtonVlissUpdates:SetColor( Color( 255,255,255,255 ) )
    self.ButtonVlissUpdates:SetFont("VlissAboutUpdateText")
    self.ButtonVlissUpdates:SetSize(200, 30)
    self.ButtonVlissUpdates:DockMargin(4,4,4,4)
    self.ButtonVlissUpdates:Dock(BOTTOM)
    self.ButtonVlissUpdates:SetTooltip(Vliss.Language.scan_for_available_updates)

    if LocalPlayer():IsSuperAdmin() then
        self.ButtonVlissUpdates:SetVisible(true)
    else
        self.ButtonVlissUpdates:SetVisible(false)
    end

    self.ButtonVlissUpdates.DoClick = function()

        self.ButtonVlissUpdates:SetText(Vliss.Language.searching_for_update)

        net.Start("VlissScriptUpdate")
        net.WriteEntity(pl)
        net.SendToServer()

        self.UpdateButtonStatus = Vliss.Language.please_wait

        net.Receive("VlissScriptUpdate", function()

            VerCheckResult = net.ReadString()
            VerCheckRelease = net.ReadString()

            if IsValid(self.ButtonVlissUpdates) then
                timer.Simple( 4, function()
                    local VlissUpdateAvail = false
                    local VlissVerRelease = VerCheckRelease
                    local VlissVerAvail = VerCheckResult
                    local VlissVerCurrent = Vliss.Script.Build

                    if VlissVerAvail > VlissVerCurrent then
                        VlissVerCurrent = true
                    end

                    if VlissUpdateAvail then
                        self.ButtonVlissUpdates:SetText(Vliss.Language.view_update_details)
                        self.ButtonVlissUpdates.Paint = function( self, w, h )

                            self.UpdateButtonStatus = Vliss.Language.check_for_updates

                            local txtColor = Color( 255, 255, 255, 100)
                            local boxColor = Color( 0, 100, 0, 150 )
                            local lineColor = Color( 50, 50, 50, 150 )
                            if self:IsHovered() or self:IsDown() then
                                txtColor = Color( 255, 255, 255, 200 )
                                boxColor = Color( 0, 50, 0, 200 )
                                lineColor = Color( 50, 50, 50, 210 ) 
                            end
                            draw.RoundedBox(0, 0, 0, w, h, boxColor)
                            surface.SetDrawColor(lineColor)
                            surface.DrawOutlinedRect(0, 0, w, h)

                        end
                        self.ButtonVlissUpdates.DoClick = function()
                            if GetLicenseType == 1 then
                                gui.OpenURL( "https://scriptfodder.com/scripts/view/1661/versions/" )
                            elseif GetLicenseType == 2 then
                                gui.OpenURL( "https://github.com/IAMRichardT/VlissScoreboardLite/" )
                            end
                        end

                        self.DLabelAVVerAvailable:SetText( "v" .. VlissVerAvail .. " (" .. VlissVerRelease ..") " .. Vliss.Language.obtain_update_instructions )
                        self.DLabelAVVerAvailable:SetVisible(true)
                    else
                        self.ButtonVlissUpdates:SetText(Vliss.Language.no_update_available)
                    end
                end)
            end

        end)

    end

    self.ButtonVlissUpdates.Paint = function( self, w, h )

        self.UpdateButtonStatus = Vliss.Language.check_for_updates

        local txtColor = Color( 255, 255, 255, 100)
        local boxColor = Color( 0, 0, 0, 150 )
        local lineColor = Color( 50, 50, 50, 150 )
        if self:IsHovered() or self:IsDown() then
            txtColor = Color( 255, 255, 255, 200 )
            boxColor = Color( 0, 0, 0, 200 )
            lineColor = Color( 50, 50, 50, 210 ) 
        end
        draw.RoundedBox(0, 0, 0, w, h, boxColor)
        surface.SetDrawColor(lineColor)
        surface.DrawOutlinedRect(0, 0, w, h)

    end

end

vgui.Register( 'vliss_panel_about', PANEL, 'EditablePanel' )