-----------------------------------------------------------------
-- @package     Vliss
-- @authors     Richard
-- @build       v1.4.0
-- @release     12.29.2015
-----------------------------------------------------------------

-----------------------------------------------------------------
-- [ HOME BUTTONS ]
-----------------------------------------------------------------
-- Displayed on left-side of scoreboard
-- You can disable any one of these by changing 
-- enabled = true to false.
--
-- To open a URL:
--      Vliss:OpenURL("http://url.com", "Titlebar Text")
--
-- To open standard text:
--      Vliss:OpenText("Text here", "Titlebar Text")
--
-----------------------------------------------------------------

Vliss.UseMenuIconsWithText = true

Vliss.MenuLinkDonate = "http://facepunch.com/"
Vliss.MenuLinkWebsite = "http://facepunch.com/"
Vliss.MenuLinkWorkshop = "http://facepunch.com/"

Vliss.Buttons = {
    {
        name = "Donate",
        description = "Donate to help keep us running",
        icon = "vliss/vliss_btn_donate.png",
        buttonNormal = Color(64, 105, 126, 190),
        buttonHover = Color(64, 105, 126, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Vliss:OpenURL(Vliss.MenuLinkDonate, "Donate to our Network!")
        end
    },
    {
        name = "Website",
        description = "Visit the official website",
        icon = "vliss/vliss_btn_website.png",
        buttonNormal = Color(163, 135, 79, 190),
        buttonHover = Color(163, 135, 79, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Vliss:OpenURL(Vliss.MenuLinkWebsite, "Welcome to our Official Website!")
        end
    },
    {
        name = "Steam Collection",
        description = "Download our addons here",
        icon = "vliss/vliss_btn_workshop.png",
        buttonNormal = Color(145, 71, 101, 190),
        buttonHover = Color(145, 71, 101, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            Vliss:OpenURL(Vliss.MenuLinkWorkshop, "The Official Network Steam Collection")
        end
    },
    {
        name = "Disconnect",
        description = "Disconnect from our server",
        icon = "vliss/vliss_btn_disconnect.png",
        buttonNormal = Color(124, 51, 50, 190),
        buttonHover = Color(124, 51, 50, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            RunConsoleCommand("disconnect")
        end
    }
}

-----------------------------------------------------------------
-- [ ACTION BUTTONS ]
-----------------------------------------------------------------
-- Displayed on left-side of scoreboard
-----------------------------------------------------------------

Vliss.Actions = {
    {
        name = "Cleanup Props",
        description = "Remove all your props",
        icon = "vliss/vliss_btn_cleanup.png",
        buttonNormal = Color(64, 105, 126, 190),
        buttonHover = Color(64, 105, 126, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            RunConsoleCommand("gmod_cleanup")
        end
    },
    {
        name = "Stop Sound",
        description = "Clear local sounds",
        icon = "vliss/vliss_btn_stopsound.png",
        buttonNormal = Color(64, 105, 126, 190),
        buttonHover = Color(64, 105, 126, 240),
        textNormal = Color(255, 255, 255, 255),
        textHover = Color(255, 255, 255, 255),
        enabled = true,
        func = function()
            RunConsoleCommand("stopsound")
        end
    }
}

-----------------------------------------------------------------
-- [ CONTROL BUTTONS ]
-----------------------------------------------------------------
-- These are pretty much set for your own specifications based 
-- on what features your server has. I like to use a certain
-- color scheme for the buttons, so if you would like the same;
-- I've listed the colors below:
-----------------------------------------------------------------
-- Red              Color( 124, 51, 50, 190 )
-- Yellow/Gold      Color( 163, 135, 79, 190 )
-- Blue             Color( 64, 105, 126, 190 )
-- Fuschia          Color( 145, 71, 101, 190 )
-- Green            Color( 72, 112, 58, 190 )
-- Brown            Color( 112, 87, 58, 190 )
-----------------------------------------------------------------
-- You can also execute commands on players by forcing them to 
-- run a say command. It's the same as them doing an actin
-- themself.
-- RunConsoleCommand("say", "/placelaws")
-----------------------------------------------------------------

Vliss.Controls = {
    {
        control = "Q",
        description = "Context Menu",
        color = Color(124, 51, 50, 190),
        colorHover = Color(124, 51, 50, 240),
        enabled = true,
        func = function()
            RunConsoleCommand("+menu_context")

            if vliss_sb_init then vliss_sb_init:SetVisible(false) end
        end
    },
    {
        control = "Z",
        description = "Undo",
        color = Color(163, 135, 79, 190),
        colorHover = Color(163, 135, 79, 240),
        enabled = true,
        func = function()
            RunConsoleCommand("undo")
        end
    },
    {
        control = "C",
        description = "PAC Editor",
        color = Color(64, 105, 126, 190),
        colorHover = Color(64, 105, 126, 240),
        enabled = true,
        func = function()
            RunConsoleCommand("+menu")
        end
    },
    {
        control = "F4",
        description = "Pointshop",
        color = Color(145, 71, 101, 190),
        colorHover = Color(145, 71, 101, 240),
        enabled = true,
        func = function()
            RunConsoleCommand("+menu")
        end
    }
}
