DarkRP.createShipment("[10st] Glock 20", {
	model = "models/weapons/w_glock20.mdl",
	entity = "fas2_glock20",
	price = 4967,
	amount = 10,
	allowed = TEAM_VF,
	category = "Pistoler",
})

DarkRP.createShipment("[10st] Desert Eagle", {
	model = "models/weapons/world/pistols/w_deserteagle.mdl",
	entity = "fas2_deagle",
	price = 7920,
	amount = 10,
	allowed = TEAM_VF,
	category = "Pistoler",
})

DarkRP.createShipment("[10st] M1911", {
	model = "models/weapons/world/pistols/w_1911.mdl",
	entity = "fas2_m1911",
	price = 2050,
	amount = 10,
	allowed = TEAM_VF,
	category = "Pistoler",
})

DarkRP.createShipment("[10st] OTS-33 Pernach", {
	model = "models/weapons/world/pistols/ots33.mdl",
	entity = "fas2_ots33",
	price = 5144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Pistoler",
})

DarkRP.createShipment("[10st] P226", {
	model = "models/weapons/view/pistols/p226.mdl",
	entity = "fas2_p226",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Pistoler",
})

DarkRP.createShipment("[10st] Raging Bull", {
	model = "models/weapons/view/pistols/ragingbull.mdl",
	entity = "fas2_ragingbull",
	price = 5290,
	amount = 10,
	allowed = TEAM_VF,
	category = "Pistoler",
})

DarkRP.createShipment("[10st] AK-12", {
	model = "models/weapons/world/rifles/ak12.mdl",
	entity = "fas2_ak12",
	price = 2599,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] AK-47", {
	model = "models/weapons/w_ak47.mdl",
	entity = "fas2_ak47",
	price = 2299,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] AK-74", {
	model = "models/weapons/view/rifles/ak47.mdl",
	entity = "fas2_ak74",
	price = 2999,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] AN-94", {
	model = "models/weapons/view/rifles/an94.mdl",
	entity = "fas2_an94",
	price = 3199,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] Famas", {
	model = "models/weapons/w_famas.mdl",
	entity = "fas2_famas",
	price = 4999,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] G36C", {
	model = "models/weapons/view/rifles/g36c.mdl",
	entity = "fas2_g36c",
	price = 3544,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] G3A3", {
	model = "models/weapons/view/rifles/g3.mdl",
	entity = "fas2_g3",
	price = 3754,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] IMI Galil", {
	model = "models/weapons/view/rifles/galil.mdl",
	entity = "fas2_galil",
	price = 3200,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[10st] IMI Uzi", {
	model = "models/weapons/view/smgs/uzi.mdl",
	entity = "fas2_uzi",
	price = 3345,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] KS-23", {
	model = "models/weapons/world/shotguns/ks23.mdl",
	entity = "fas2_ks23",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Hagelgevär",
})

DarkRP.createShipment("[3st] M14", {
	model = "models/weapons/w_m14.mdl",
	entity = "fas2_m14",
	price = 2500,
	amount = 10,
	allowed = TEAM_VF,
	category = "Snipers",
})

DarkRP.createShipment("[3st] M21", {
	model = "models/weapons/view/support/m21.mdl",
	entity = "fas2_m21",
	price = 2700,
	amount = 10,
	allowed = TEAM_VF,
	category = "Snipers",
})

DarkRP.createShipment("[3st] M24", {
	model = "models/weapons/w_m24.mdl",
	entity = "fas2_m24",
	price = 5000,
	amount = 10,
	allowed = TEAM_VF,
	category = "Snipers",
})

DarkRP.createShipment("[3st] M3 Super 90", {
	model = "models/weapons/view/shotguns/m3s90.mdl",
	entity = "fas2_m3s90",
	price = 5500,
	amount = 10,
	allowed = TEAM_VF,
	category = "Hagelgevär",
})

DarkRP.createShipment("[3st] M4A1", {
	model = "models/weapons/view/rifles/m4a1.mdl",
	entity = "fas2_m4a1",
	price = 2500,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] MP5A5", {
	model = "models/weapons/view/smgs/mp5a5.mdl",
	entity = "fas2_mp5a5",
	price = 1344,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] MP5K", {
	model = "models/weapons/view/smgs/mp5k.mdl",
	entity = "fas2_mp5k",
	price = 1544,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] MP5SD6", {
	model = "models/weapons/view/smgs/mp5sd6.mdl",
	entity = "fas2_mp5sd6",
	price = 2154,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] PP-19 Bizon", {
	model = "models/weapons/view/smgs/bizon.mdl",
	entity = "fas2_pp19",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Hagelgevär",
})

DarkRP.createShipment("[3st] Remington 870", {
	model = "models/weapons/view/shotguns/870.mdl",
	entity = "fas2_rem870",
	price = 2674,
	amount = 10,
	allowed = TEAM_VF,
	category = "Hagelgevär",
})

DarkRP.createShipment("[3st] RPK-47", {
	model = "models/weapons/view/support/rpk.mdl",
	entity = "fas2_rpk",
	price = 544,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] Sako RK-95", {
	model = "models/weapons/world/rifles/rk95.mdl",
	entity = "fas2_rk95",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] SG 550", {
	model = "models/weapons/w_sg550.mdl",
	entity = "fas2_sg550",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] SG 552", {
	model = "models/weapons/view/rifles/sg552.mdl",
	entity = "fas2_sg552",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Gevär",
})

DarkRP.createShipment("[3st] SKS", {
	model = "models/weapons/world/rifles/sks.mdl",
	entity = "fas2_sks",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Snipers",
})

DarkRP.createShipment("[3st] TOZ-34", {
	model = "models/weapons/view/shotguns/toz34.mdl",
	entity = "fas2_toz34",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Hagelgevär",
})

DarkRP.createShipment("[3st] M82", {
	model = "models/weapons/w_m82.mdl",
	entity = "fas2_m82",
	price = 3144,
	amount = 10,
	allowed = TEAM_VF,
	category = "Snipers",
})

DarkRP.createShipment("DV-2", {
	model = "models/weapons/w_dv2.mdl",
	entity = "fas2_dv2",
	price = 0,
	amount = 0,
	separate = true,
	pricesep = 205,
	noship = true,
	category = "Knivar",
})

DarkRP.createShipment("Machete", {
	model = "models/weapons/w_machete.mdl",
	entity = "fas2_machete",
	price = 0,
	amount = 0,
	separate = true,
	pricesep = 205,
	noship = true,
	category = "Knivar",
})

DarkRP.createShipment("M1911", {
	model = "models/weapons/world/pistols/w_1911.mdl",
	entity = "fas2_m1911",
	price = 0,
	amount = 0,
	separate = true,
	pricesep = 205,
	noship = true,
	category = "Vapen",
})

DarkRP.createShipment("First Aid Kit", {
	model = "models/weapons/w_ifak.mdl",
	entity = "fas2_ifak",
	price = 0,
	amount = 0,
	separate = true,
	pricesep = 205,
	noship = true,
	category = "Annat",
})